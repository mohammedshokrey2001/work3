package com.example.chatapp.domain.models.chat;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\n\bf\u0018\u0000 \f2\u00020\u0001:\u0001\fR\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\u0005R\u0012\u0010\b\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\u0005R\u0012\u0010\n\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000b\u0010\u0005\u00a8\u0006\r"}, d2 = {"Lcom/example/chatapp/domain/models/chat/Message;", "", "receiverId", "", "getReceiverId", "()Ljava/lang/String;", "senderId", "getSenderId", "time", "getTime", "type", "getType", "Companion", "app_pilot"})
public abstract interface Message {
    @org.jetbrains.annotations.NotNull
    public static final com.example.chatapp.domain.models.chat.Message.Companion Companion = null;
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String Text = "TEXT";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String IMAGE = "IMAGE";
    
    @org.jetbrains.annotations.NotNull
    public abstract java.lang.String getTime();
    
    @org.jetbrains.annotations.NotNull
    public abstract java.lang.String getSenderId();
    
    @org.jetbrains.annotations.NotNull
    public abstract java.lang.String getType();
    
    @org.jetbrains.annotations.NotNull
    public abstract java.lang.String getReceiverId();
    
    @kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/example/chatapp/domain/models/chat/Message$Companion;", "", "()V", "IMAGE", "", "Text", "app_pilot"})
    public static final class Companion {
        @org.jetbrains.annotations.NotNull
        public static final java.lang.String Text = "TEXT";
        @org.jetbrains.annotations.NotNull
        public static final java.lang.String IMAGE = "IMAGE";
        
        private Companion() {
            super();
        }
    }
}