package com.example.chatapp.ui.screens;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\r\u001a\u00020\u000eH\u0002J\u0012\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0014J\u0006\u0010\u0013\u001a\u00020\u0010R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0007\u001a\u00020\b8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/example/chatapp/ui/screens/MainActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "binding", "Lcom/example/chatapp/databinding/ActivityMainBinding;", "navController", "Landroidx/navigation/NavController;", "viewModel", "Lcom/example/chatapp/ui/view_model/AppViewModel;", "getViewModel", "()Lcom/example/chatapp/ui/view_model/AppViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "checkPermissionForReadExtertalStorage", "", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "requestPermissionForReadExtertalStorage", "Companion", "app_unpaindDebug"})
public final class MainActivity extends androidx.appcompat.app.AppCompatActivity {
    private com.example.chatapp.databinding.ActivityMainBinding binding;
    private androidx.navigation.NavController navController;
    private final kotlin.Lazy viewModel$delegate = null;
    @org.jetbrains.annotations.NotNull
    public static final com.example.chatapp.ui.screens.MainActivity.Companion Companion = null;
    private static final int STORAGE_PERMISSION_CODE = 2005;
    @org.jetbrains.annotations.NotNull
    private static final java.lang.String TAG = "ACCESS";
    private static final int READ_STORAGE_PERMISSION_REQUEST_CODE = 41;
    
    public MainActivity() {
        super();
    }
    
    private final com.example.chatapp.ui.view_model.AppViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override
    protected void onCreate(@org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
    }
    
    private final boolean checkPermissionForReadExtertalStorage() {
        return false;
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.Exception.class})
    public final void requestPermissionForReadExtertalStorage() throws java.lang.Exception {
    }
    
    @kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0014\u0010\t\u001a\u00020\nX\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\r"}, d2 = {"Lcom/example/chatapp/ui/screens/MainActivity$Companion;", "", "()V", "READ_STORAGE_PERMISSION_REQUEST_CODE", "", "getREAD_STORAGE_PERMISSION_REQUEST_CODE", "()I", "STORAGE_PERMISSION_CODE", "getSTORAGE_PERMISSION_CODE", "TAG", "", "getTAG", "()Ljava/lang/String;", "app_unpaindDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        public final int getSTORAGE_PERMISSION_CODE() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull
        public final java.lang.String getTAG() {
            return null;
        }
        
        public final int getREAD_STORAGE_PERMISSION_REQUEST_CODE() {
            return 0;
        }
    }
}