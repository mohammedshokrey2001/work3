package com.example.chatapp.ui.binding_adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B#\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\b\u0010\u0016\u001a\u00020\u0017H\u0016J\u0018\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u0017H\u0016J\u0018\u0010\u001c\u001a\u00020\u00022\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0017H\u0016R\u001a\u0010\u000b\u001a\u00020\fX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R \u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "}, d2 = {"Lcom/example/chatapp/ui/binding_adapters/UserListAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/example/chatapp/ui/binding_adapters/UserViewHolder;", "usersList", "", "Lcom/example/chatapp/domain/models/user/User;", "navController", "Landroidx/navigation/NavController;", "viewModel", "Lcom/example/chatapp/ui/view_model/AppViewModel;", "(Ljava/util/List;Landroidx/navigation/NavController;Lcom/example/chatapp/ui/view_model/AppViewModel;)V", "binding", "Lcom/example/chatapp/databinding/ListItemUserBinding;", "getBinding", "()Lcom/example/chatapp/databinding/ListItemUserBinding;", "setBinding", "(Lcom/example/chatapp/databinding/ListItemUserBinding;)V", "data", "getData", "()Ljava/util/List;", "setData", "(Ljava/util/List;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "app_unpaindDebug"})
public final class UserListAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.example.chatapp.ui.binding_adapters.UserViewHolder> {
    private final java.util.List<com.example.chatapp.domain.models.user.User> usersList = null;
    private final androidx.navigation.NavController navController = null;
    private final com.example.chatapp.ui.view_model.AppViewModel viewModel = null;
    @org.jetbrains.annotations.NotNull
    private java.util.List<com.example.chatapp.domain.models.user.User> data;
    public com.example.chatapp.databinding.ListItemUserBinding binding;
    
    public UserListAdapter(@org.jetbrains.annotations.NotNull
    java.util.List<com.example.chatapp.domain.models.user.User> usersList, @org.jetbrains.annotations.NotNull
    androidx.navigation.NavController navController, @org.jetbrains.annotations.NotNull
    com.example.chatapp.ui.view_model.AppViewModel viewModel) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.util.List<com.example.chatapp.domain.models.user.User> getData() {
        return null;
    }
    
    public final void setData(@org.jetbrains.annotations.NotNull
    java.util.List<com.example.chatapp.domain.models.user.User> p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final com.example.chatapp.databinding.ListItemUserBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull
    com.example.chatapp.databinding.ListItemUserBinding p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public com.example.chatapp.ui.binding_adapters.UserViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull
    com.example.chatapp.ui.binding_adapters.UserViewHolder holder, int position) {
    }
}