package com.example.chatapp.firebase;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/example/chatapp/firebase/Firebase;", "", "()V", "app_release"})
public final class Firebase {
    @org.jetbrains.annotations.NotNull
    public static final com.example.chatapp.firebase.Firebase INSTANCE = null;
    
    private Firebase() {
        super();
    }
}