package com.example.chatapp.ui.view_model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0016\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u00101\u001a\u0002022\u0006\u00103\u001a\u0002042\u0006\u00105\u001a\u000206J\u000e\u00107\u001a\u0002082\u0006\u00109\u001a\u00020\u0006J\u000e\u0010:\u001a\u0002082\u0006\u0010;\u001a\u000202J\u0006\u0010<\u001a\u000208J\u0006\u0010=\u001a\u000208J\u0006\u0010>\u001a\u000208J\u000e\u0010?\u001a\u0002082\u0006\u0010@\u001a\u00020AJ\u0006\u0010B\u001a\u000208J\u0016\u0010C\u001a\u0002082\u0006\u0010D\u001a\u00020E2\u0006\u00105\u001a\u000206J\u0016\u0010F\u001a\u0002082\u0006\u00103\u001a\u0002042\u0006\u00105\u001a\u000206R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R(\u0010\u0014\u001a\u0010\u0012\f\u0012\n \u0017*\u0004\u0018\u00010\u00160\u00160\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u001a\u0010\u001c\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u001a\u0010!\u001a\u00020\u000fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R(\u0010&\u001a\u0010\u0012\f\u0012\n \u0017*\u0004\u0018\u00010\u00160\u00160\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\'\u0010\u0019\"\u0004\b(\u0010\u001bR(\u0010)\u001a\u0010\u0012\f\u0012\n \u0017*\u0004\u0018\u00010\u00160\u00160\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010\u0019\"\u0004\b+\u0010\u001bR\u000e\u0010,\u001a\u00020-X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010.\u001a\u00020\u000fX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u0010#\"\u0004\b0\u0010%\u00a8\u0006G"}, d2 = {"Lcom/example/chatapp/ui/view_model/AppViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "PREFERENCES_NAME_USER", "", "TAG", "auth", "Lcom/google/firebase/auth/FirebaseAuth;", "currentUserId", "database", "Lcom/google/firebase/database/DatabaseReference;", "listOfUsers", "", "Lcom/example/chatapp/domain/models/user/User;", "getListOfUsers", "()Ljava/util/List;", "setListOfUsers", "(Ljava/util/List;)V", "newUsersDone", "Landroidx/lifecycle/MutableLiveData;", "", "kotlin.jvm.PlatformType", "getNewUsersDone", "()Landroidx/lifecycle/MutableLiveData;", "setNewUsersDone", "(Landroidx/lifecycle/MutableLiveData;)V", "otherId", "getOtherId", "()Ljava/lang/String;", "setOtherId", "(Ljava/lang/String;)V", "otherUserMessaging", "getOtherUserMessaging", "()Lcom/example/chatapp/domain/models/user/User;", "setOtherUserMessaging", "(Lcom/example/chatapp/domain/models/user/User;)V", "signInDone", "getSignInDone", "setSignInDone", "signUpDone", "getSignUpDone", "setSignUpDone", "storage", "Lcom/google/firebase/storage/FirebaseStorage;", "user", "getUser", "setUser", "assitance", "", "signUpModel", "Lcom/example/chatapp/domain/models/auth/SignUpModel;", "context", "Landroid/content/Context;", "changeStatus", "", "status", "creteNewProfileImage", "img", "getAllUsers", "getMessages", "getStatus", "sendMessage", "messageTextMessageModel", "Lcom/example/chatapp/domain/models/chat/TextMessageModel;", "setData", "signIn", "signInModel", "Lcom/example/chatapp/domain/models/auth/SignInModel;", "signUp", "app_release"})
public final class AppViewModel extends androidx.lifecycle.AndroidViewModel {
    private com.google.firebase.auth.FirebaseAuth auth;
    @org.jetbrains.annotations.NotNull
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> signUpDone;
    @org.jetbrains.annotations.NotNull
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> signInDone;
    @org.jetbrains.annotations.NotNull
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> newUsersDone;
    private final com.google.firebase.storage.FirebaseStorage storage = null;
    private final java.lang.String TAG = "ViewModel";
    public com.example.chatapp.domain.models.user.User user;
    private java.lang.String currentUserId = "";
    @org.jetbrains.annotations.NotNull
    private com.example.chatapp.domain.models.user.User otherUserMessaging;
    private final java.lang.String PREFERENCES_NAME_USER = "sample_datastore_prefs";
    @org.jetbrains.annotations.NotNull
    private java.lang.String otherId = "";
    private com.google.firebase.database.DatabaseReference database;
    @org.jetbrains.annotations.NotNull
    private java.util.List<com.example.chatapp.domain.models.user.User> listOfUsers;
    
    public AppViewModel(@org.jetbrains.annotations.NotNull
    android.app.Application application) {
        super(null);
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getSignUpDone() {
        return null;
    }
    
    public final void setSignUpDone(@org.jetbrains.annotations.NotNull
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getSignInDone() {
        return null;
    }
    
    public final void setSignInDone(@org.jetbrains.annotations.NotNull
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getNewUsersDone() {
        return null;
    }
    
    public final void setNewUsersDone(@org.jetbrains.annotations.NotNull
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final com.example.chatapp.domain.models.user.User getUser() {
        return null;
    }
    
    public final void setUser(@org.jetbrains.annotations.NotNull
    com.example.chatapp.domain.models.user.User p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final com.example.chatapp.domain.models.user.User getOtherUserMessaging() {
        return null;
    }
    
    public final void setOtherUserMessaging(@org.jetbrains.annotations.NotNull
    com.example.chatapp.domain.models.user.User p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.lang.String getOtherId() {
        return null;
    }
    
    public final void setOtherId(@org.jetbrains.annotations.NotNull
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.util.List<com.example.chatapp.domain.models.user.User> getListOfUsers() {
        return null;
    }
    
    public final void setListOfUsers(@org.jetbrains.annotations.NotNull
    java.util.List<com.example.chatapp.domain.models.user.User> p0) {
    }
    
    public final void signUp(@org.jetbrains.annotations.NotNull
    com.example.chatapp.domain.models.auth.SignUpModel signUpModel, @org.jetbrains.annotations.NotNull
    android.content.Context context) {
    }
    
    public final void signIn(@org.jetbrains.annotations.NotNull
    com.example.chatapp.domain.models.auth.SignInModel signInModel, @org.jetbrains.annotations.NotNull
    android.content.Context context) {
    }
    
    public final void creteNewProfileImage(@org.jetbrains.annotations.NotNull
    byte[] img) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final byte[] assitance(@org.jetbrains.annotations.NotNull
    com.example.chatapp.domain.models.auth.SignUpModel signUpModel, @org.jetbrains.annotations.NotNull
    android.content.Context context) {
        return null;
    }
    
    public final void setData() {
    }
    
    public final void changeStatus(@org.jetbrains.annotations.NotNull
    java.lang.String status) {
    }
    
    public final void getStatus() {
    }
    
    public final void getAllUsers() {
    }
    
    public final void getMessages() {
    }
    
    public final void sendMessage(@org.jetbrains.annotations.NotNull
    com.example.chatapp.domain.models.chat.TextMessageModel messageTextMessageModel) {
    }
}