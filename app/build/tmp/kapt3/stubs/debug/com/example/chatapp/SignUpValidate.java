package com.example.chatapp;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0002J\u0016\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\u0006J\u000e\u0010\n\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\f\u00a8\u0006\r"}, d2 = {"Lcom/example/chatapp/SignUpValidate;", "", "()V", "isValidPassword", "", "password", "", "validPass", "pass", "repass", "validate", "signUpModel", "Lcom/example/chatapp/domain/models/auth/SignUpModel;", "app_debug"})
public final class SignUpValidate {
    @org.jetbrains.annotations.NotNull
    public static final com.example.chatapp.SignUpValidate INSTANCE = null;
    
    private SignUpValidate() {
        super();
    }
    
    public final boolean validate(@org.jetbrains.annotations.NotNull
    com.example.chatapp.domain.models.auth.SignUpModel signUpModel) {
        return false;
    }
    
    public final boolean validPass(@org.jetbrains.annotations.NotNull
    java.lang.String pass, @org.jetbrains.annotations.NotNull
    java.lang.String repass) {
        return false;
    }
    
    private final boolean isValidPassword(java.lang.String password) {
        return false;
    }
}