package com.example.chatapp.domain.shared_pref;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u0000  2\u00020\u0001:\u0001 B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\t\u001a\u00020\nJ\u0012\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0002J\u0012\u0010\r\u001a\u00020\u000e2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0002J\u0019\u0010\u000f\u001a\u0004\u0018\u00010\u00102\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0002\u00a2\u0006\u0002\u0010\u0011J\u0006\u0010\u0012\u001a\u00020\u000eJ\u0010\u0010\u0013\u001a\u00020\f2\u0006\u0010\u000b\u001a\u00020\fH\u0002J\u0006\u0010\u0014\u001a\u00020\u0015J!\u0010\u0016\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\f2\b\u0010\u0017\u001a\u0004\u0018\u00010\u000eH\u0002\u00a2\u0006\u0002\u0010\u0018J!\u0010\u0019\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\f2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0010H\u0002\u00a2\u0006\u0002\u0010\u001aJ\u000e\u0010\u001b\u001a\u00020\n2\u0006\u0010\u001c\u001a\u00020\u000eJ\u001c\u0010\u001d\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\f2\b\u0010\u0017\u001a\u0004\u0018\u00010\fH\u0002J\u000e\u0010\u001e\u001a\u00020\n2\u0006\u0010\u001f\u001a\u00020\u0015R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"}, d2 = {"Lcom/example/chatapp/domain/shared_pref/AppSharedPref;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "editor", "Landroid/content/SharedPreferences$Editor;", "sharedPreferences", "Landroid/content/SharedPreferences;", "clear", "", "key", "", "getBoolean", "", "getInt", "", "(Ljava/lang/String;)Ljava/lang/Integer;", "getLoggedInState", "getString", "getUserData", "Lcom/example/chatapp/domain/models/user/User;", "setBoolean", "value", "(Ljava/lang/String;Ljava/lang/Boolean;)V", "setInt", "(Ljava/lang/String;Ljava/lang/Integer;)V", "setLoggedInState", "state", "setString", "setUserData", "user", "Companion", "app_debug"})
public final class AppSharedPref {
    private final android.content.SharedPreferences sharedPreferences = null;
    private final android.content.SharedPreferences.Editor editor = null;
    @org.jetbrains.annotations.NotNull
    public static final com.example.chatapp.domain.shared_pref.AppSharedPref.Companion Companion = null;
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String MY_PREF = "MyPreferences";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String LOGGED_IN_STATE = "log_in_state";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String NAME = "name";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String STATUS = "status";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String ID = "id";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String PROFILE_IMAGE = "profile_img_url";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String MAIL = "mail";
    
    public AppSharedPref(@org.jetbrains.annotations.NotNull
    android.content.Context context) {
        super();
    }
    
    private final void setInt(java.lang.String key, java.lang.Integer value) {
    }
    
    private final void setString(java.lang.String key, java.lang.String value) {
    }
    
    private final void setBoolean(java.lang.String key, java.lang.Boolean value) {
    }
    
    private final boolean getBoolean(java.lang.String key) {
        return false;
    }
    
    private final java.lang.Integer getInt(java.lang.String key) {
        return null;
    }
    
    private final java.lang.String getString(java.lang.String key) {
        return null;
    }
    
    private final void clear(java.lang.String key) {
    }
    
    public final void clear() {
    }
    
    public final boolean getLoggedInState() {
        return false;
    }
    
    public final void setLoggedInState(boolean state) {
    }
    
    public final void setUserData(@org.jetbrains.annotations.NotNull
    com.example.chatapp.domain.models.user.User user) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final com.example.chatapp.domain.models.user.User getUserData() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/example/chatapp/domain/shared_pref/AppSharedPref$Companion;", "", "()V", "ID", "", "LOGGED_IN_STATE", "MAIL", "MY_PREF", "NAME", "PROFILE_IMAGE", "STATUS", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}