package com.example.chatapp.ui.binding_adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/example/chatapp/ui/binding_adapters/ChatViewHolderSent;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "binding", "Lcom/example/chatapp/databinding/ListItemMessageSentBinding;", "(Lcom/example/chatapp/databinding/ListItemMessageSentBinding;)V", "bind", "", "message", "Lcom/example/chatapp/domain/models/chat/ChatMessage;", "app_paidPilot"})
public final class ChatViewHolderSent extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
    private final com.example.chatapp.databinding.ListItemMessageSentBinding binding = null;
    
    public ChatViewHolderSent(@org.jetbrains.annotations.NotNull
    com.example.chatapp.databinding.ListItemMessageSentBinding binding) {
        super(null);
    }
    
    public final void bind(@org.jetbrains.annotations.NotNull
    com.example.chatapp.domain.models.chat.ChatMessage message) {
    }
}