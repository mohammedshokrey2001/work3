package com.example.chatapp.ui.binding_adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 2, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0007\u00a8\u0006\u0005"}, d2 = {"setImageFromUrl", "", "Landroid/widget/ImageView;", "url", "", "app_paidPilot"})
public final class AdaptersKt {
    
    @androidx.databinding.BindingAdapter(value = {"bind_image_url_blur"}, requireAll = true)
    public static final void setImageFromUrl(@org.jetbrains.annotations.NotNull
    android.widget.ImageView $this$setImageFromUrl, @org.jetbrains.annotations.NotNull
    java.lang.String url) {
    }
}