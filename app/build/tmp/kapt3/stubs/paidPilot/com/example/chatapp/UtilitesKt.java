package com.example.chatapp;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 2, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0010\b\n\u0002\b\u0005\" \u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0003\u0010\u0004\"\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"DestWithNoNavigationBarMenuView", "", "", "getDestWithNoNavigationBarMenuView", "()Ljava/util/List;", "setDestWithNoNavigationBarMenuView", "(Ljava/util/List;)V", "app_paidPilot"})
public final class UtilitesKt {
    @org.jetbrains.annotations.NotNull
    private static java.util.List<java.lang.Integer> DestWithNoNavigationBarMenuView;
    
    @org.jetbrains.annotations.NotNull
    public static final java.util.List<java.lang.Integer> getDestWithNoNavigationBarMenuView() {
        return null;
    }
    
    public static final void setDestWithNoNavigationBarMenuView(@org.jetbrains.annotations.NotNull
    java.util.List<java.lang.Integer> p0) {
    }
}