package com.example.chatapp;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.example.chatapp.databinding.ActivityMainBindingImpl;
import com.example.chatapp.databinding.FragmentChatBindingImpl;
import com.example.chatapp.databinding.FragmentCreateAccountBindingImpl;
import com.example.chatapp.databinding.FragmentPeopleBindingImpl;
import com.example.chatapp.databinding.FragmentProfileBindingImpl;
import com.example.chatapp.databinding.FragmentSignInBindingImpl;
import com.example.chatapp.databinding.FragmentStartBindingImpl;
import com.example.chatapp.databinding.ListItemMessageReceivedBindingImpl;
import com.example.chatapp.databinding.ListItemMessageSentBindingImpl;
import com.example.chatapp.databinding.ListItemUserBindingImpl;
import com.example.chatapp.databinding.ToolbarAddonChatBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYMAIN = 1;

  private static final int LAYOUT_FRAGMENTCHAT = 2;

  private static final int LAYOUT_FRAGMENTCREATEACCOUNT = 3;

  private static final int LAYOUT_FRAGMENTPEOPLE = 4;

  private static final int LAYOUT_FRAGMENTPROFILE = 5;

  private static final int LAYOUT_FRAGMENTSIGNIN = 6;

  private static final int LAYOUT_FRAGMENTSTART = 7;

  private static final int LAYOUT_LISTITEMMESSAGERECEIVED = 8;

  private static final int LAYOUT_LISTITEMMESSAGESENT = 9;

  private static final int LAYOUT_LISTITEMUSER = 10;

  private static final int LAYOUT_TOOLBARADDONCHAT = 11;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(11);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.chatapp.R.layout.activity_main, LAYOUT_ACTIVITYMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.chatapp.R.layout.fragment_chat, LAYOUT_FRAGMENTCHAT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.chatapp.R.layout.fragment_create_account, LAYOUT_FRAGMENTCREATEACCOUNT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.chatapp.R.layout.fragment_people, LAYOUT_FRAGMENTPEOPLE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.chatapp.R.layout.fragment_profile, LAYOUT_FRAGMENTPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.chatapp.R.layout.fragment_sign_in, LAYOUT_FRAGMENTSIGNIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.chatapp.R.layout.fragment_start, LAYOUT_FRAGMENTSTART);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.chatapp.R.layout.list_item_message_received, LAYOUT_LISTITEMMESSAGERECEIVED);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.chatapp.R.layout.list_item_message_sent, LAYOUT_LISTITEMMESSAGESENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.chatapp.R.layout.list_item_user, LAYOUT_LISTITEMUSER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.chatapp.R.layout.toolbar_addon_chat, LAYOUT_TOOLBARADDONCHAT);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYMAIN: {
          if ("layout/activity_main_0".equals(tag)) {
            return new ActivityMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTCHAT: {
          if ("layout/fragment_chat_0".equals(tag)) {
            return new FragmentChatBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_chat is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTCREATEACCOUNT: {
          if ("layout/fragment_create_account_0".equals(tag)) {
            return new FragmentCreateAccountBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_create_account is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTPEOPLE: {
          if ("layout/fragment_people_0".equals(tag)) {
            return new FragmentPeopleBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_people is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTPROFILE: {
          if ("layout/fragment_profile_0".equals(tag)) {
            return new FragmentProfileBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_profile is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSIGNIN: {
          if ("layout/fragment_sign_in_0".equals(tag)) {
            return new FragmentSignInBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_sign_in is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSTART: {
          if ("layout/fragment_start_0".equals(tag)) {
            return new FragmentStartBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_start is invalid. Received: " + tag);
        }
        case  LAYOUT_LISTITEMMESSAGERECEIVED: {
          if ("layout/list_item_message_received_0".equals(tag)) {
            return new ListItemMessageReceivedBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for list_item_message_received is invalid. Received: " + tag);
        }
        case  LAYOUT_LISTITEMMESSAGESENT: {
          if ("layout/list_item_message_sent_0".equals(tag)) {
            return new ListItemMessageSentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for list_item_message_sent is invalid. Received: " + tag);
        }
        case  LAYOUT_LISTITEMUSER: {
          if ("layout/list_item_user_0".equals(tag)) {
            return new ListItemUserBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for list_item_user is invalid. Received: " + tag);
        }
        case  LAYOUT_TOOLBARADDONCHAT: {
          if ("layout/toolbar_addon_chat_0".equals(tag)) {
            return new ToolbarAddonChatBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for toolbar_addon_chat is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(4);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "message");
      sKeys.put(2, "user");
      sKeys.put(3, "viewmodel");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(11);

    static {
      sKeys.put("layout/activity_main_0", com.example.chatapp.R.layout.activity_main);
      sKeys.put("layout/fragment_chat_0", com.example.chatapp.R.layout.fragment_chat);
      sKeys.put("layout/fragment_create_account_0", com.example.chatapp.R.layout.fragment_create_account);
      sKeys.put("layout/fragment_people_0", com.example.chatapp.R.layout.fragment_people);
      sKeys.put("layout/fragment_profile_0", com.example.chatapp.R.layout.fragment_profile);
      sKeys.put("layout/fragment_sign_in_0", com.example.chatapp.R.layout.fragment_sign_in);
      sKeys.put("layout/fragment_start_0", com.example.chatapp.R.layout.fragment_start);
      sKeys.put("layout/list_item_message_received_0", com.example.chatapp.R.layout.list_item_message_received);
      sKeys.put("layout/list_item_message_sent_0", com.example.chatapp.R.layout.list_item_message_sent);
      sKeys.put("layout/list_item_user_0", com.example.chatapp.R.layout.list_item_user);
      sKeys.put("layout/toolbar_addon_chat_0", com.example.chatapp.R.layout.toolbar_addon_chat);
    }
  }
}
