package com.example.chatapp.databinding;
import com.example.chatapp.R;
import com.example.chatapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentProfileBindingImpl extends FragmentProfileBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.logoutButton, 5);
        sViewsWithIds.put(R.id.userImageCardView, 6);
        sViewsWithIds.put(R.id.contentLayout, 7);
        sViewsWithIds.put(R.id.changeImageButton, 8);
        sViewsWithIds.put(R.id.changeStatusButton, 9);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.ImageView mboundView2;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentProfileBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 10, sIncludes, sViewsWithIds));
    }
    private FragmentProfileBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[1]
            , (android.widget.Button) bindings[8]
            , (android.widget.Button) bindings[9]
            , (android.widget.LinearLayout) bindings[7]
            , (android.widget.Button) bindings[5]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[4]
            , (com.google.android.material.card.MaterialCardView) bindings[6]
            );
        this.blurredUserImage.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (android.widget.ImageView) bindings[2];
        this.mboundView2.setTag(null);
        this.nameText.setTag(null);
        this.statusText.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewmodel == variableId) {
            setViewmodel((com.example.chatapp.ui.view_model.AppViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewmodel(@Nullable com.example.chatapp.ui.view_model.AppViewModel Viewmodel) {
        this.mViewmodel = Viewmodel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.viewmodel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.example.chatapp.domain.models.user.User viewmodelUser = null;
        java.lang.String viewmodelUserProfileImageUrl = null;
        java.lang.String viewmodelUserStatus = null;
        java.lang.String viewmodelUserName = null;
        com.example.chatapp.ui.view_model.AppViewModel viewmodel = mViewmodel;

        if ((dirtyFlags & 0x3L) != 0) {



                if (viewmodel != null) {
                    // read viewmodel.user
                    viewmodelUser = viewmodel.getUser();
                }


                if (viewmodelUser != null) {
                    // read viewmodel.user.profileImageUrl
                    viewmodelUserProfileImageUrl = viewmodelUser.getProfileImageUrl();
                    // read viewmodel.user.status
                    viewmodelUserStatus = viewmodelUser.getStatus();
                    // read viewmodel.user.name
                    viewmodelUserName = viewmodelUser.getName();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.example.chatapp.ui.binding_adapters.AdaptersKt.setImageFromUrl(this.blurredUserImage, viewmodelUserProfileImageUrl);
            com.example.chatapp.ui.binding_adapters.AdaptersKt.setImageFromUrl(this.mboundView2, viewmodelUserProfileImageUrl);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.nameText, viewmodelUserName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.statusText, viewmodelUserStatus);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewmodel
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}