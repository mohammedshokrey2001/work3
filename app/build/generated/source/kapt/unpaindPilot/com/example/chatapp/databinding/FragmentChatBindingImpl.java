package com.example.chatapp.databinding;
import com.example.chatapp.R;
import com.example.chatapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentChatBindingImpl extends FragmentChatBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.relativeLayout, 3);
        sViewsWithIds.put(R.id.imageContentLayout, 4);
        sViewsWithIds.put(R.id.imageCardView, 5);
        sViewsWithIds.put(R.id.onlineView, 6);
        sViewsWithIds.put(R.id.onlineStatusText, 7);
        sViewsWithIds.put(R.id.messagesRecyclerView, 8);
        sViewsWithIds.put(R.id.view, 9);
        sViewsWithIds.put(R.id.layoutChatbox, 10);
        sViewsWithIds.put(R.id.editTextMessage, 11);
        sViewsWithIds.put(R.id.sendBtn, 12);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentChatBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 13, sIncludes, sViewsWithIds));
    }
    private FragmentChatBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.EditText) bindings[11]
            , (androidx.cardview.widget.CardView) bindings[5]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[4]
            , (android.widget.LinearLayout) bindings[10]
            , (androidx.recyclerview.widget.RecyclerView) bindings[8]
            , (android.widget.TextView) bindings[7]
            , (android.view.View) bindings[6]
            , (android.widget.TextView) bindings[2]
            , (android.widget.RelativeLayout) bindings[3]
            , (android.widget.Button) bindings[12]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            , (android.widget.ImageView) bindings[1]
            , (android.view.View) bindings[9]
            );
        this.otherUserNameText.setTag(null);
        this.textContentLayout.setTag(null);
        this.userProfileImage.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewmodel == variableId) {
            setViewmodel((com.example.chatapp.ui.view_model.AppViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewmodel(@Nullable com.example.chatapp.ui.view_model.AppViewModel Viewmodel) {
        this.mViewmodel = Viewmodel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.viewmodel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.example.chatapp.domain.models.user.User viewmodelOtherUserMessaging = null;
        java.lang.String viewmodelOtherUserMessagingProfileImageUrl = null;
        java.lang.String viewmodelOtherUserMessagingName = null;
        com.example.chatapp.ui.view_model.AppViewModel viewmodel = mViewmodel;

        if ((dirtyFlags & 0x3L) != 0) {



                if (viewmodel != null) {
                    // read viewmodel.otherUserMessaging
                    viewmodelOtherUserMessaging = viewmodel.getOtherUserMessaging();
                }


                if (viewmodelOtherUserMessaging != null) {
                    // read viewmodel.otherUserMessaging.profileImageUrl
                    viewmodelOtherUserMessagingProfileImageUrl = viewmodelOtherUserMessaging.getProfileImageUrl();
                    // read viewmodel.otherUserMessaging.name
                    viewmodelOtherUserMessagingName = viewmodelOtherUserMessaging.getName();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.otherUserNameText, viewmodelOtherUserMessagingName);
            com.example.chatapp.ui.binding_adapters.AdaptersKt.setImageFromUrl(this.userProfileImage, viewmodelOtherUserMessagingProfileImageUrl);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewmodel
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}