package com.example.chatapp


import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
internal class SignUpValidateTest {
     //test should passed
    @Test
    fun passwordValidator_pass_match_constraints_true() {
        assertTrue(SignUpValidate.validPass("MOHA_mmed_5546", "MOHA_mmed_5546"))
    }

    //test should passed
    @Test
    fun passwordValidator_pass_does_not_match_re_pass_false() {
        assertFalse(SignUpValidate.validPass("MOHA_mmed_5546", "MOHA_md_5546"))
    }

//    //test should not passed
//    @Test
//    fun passwordValidator_pass_does_not_match_re_pass_true() {
//        assertTrue(SignUpValidate.validPass("MOHA_mmed_5546", "MOHA_md_5546"))
//    }

}