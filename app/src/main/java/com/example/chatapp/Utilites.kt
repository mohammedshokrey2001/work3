package com.example.chatapp

import android.content.Context
import android.widget.Toast
import com.example.chatapp.domain.models.auth.SignUpModel
import java.util.regex.Matcher
import java.util.regex.Pattern

var DestWithNoNavigationBarMenuView = listOf<Int>(
    R.id.startFragment,
    R.id.signInFragment,
    R.id.createAccountFragment,
    R.id.chatFragment
)



object SignUpValidate {
    fun validate(signUpModel: SignUpModel): Boolean {
        if (
            validPass(signUpModel.pass,signUpModel.pass)
        ) {
            return true
        }

        return false
    }

    fun validPass(pass:String,repass:String): Boolean {
        if (!isValidPassword(password = pass)) {

            return false
        }
        if ((pass != repass)) {

            return false
        }
        return true
    }
    private fun isValidPassword(password: String?): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{4,}$"
        pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(password)
        return matcher.matches()

    }




}
